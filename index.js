// console.log("hello")


//section : DOcuments Object Model (DOM)
	//allows us to access or modify the properties of an HTML element in a webpage
	//it is standard on how to get, change, add, or delete HTML elements
	//we will fucos on use of DOM in managing forms.


//For selecting HTML elements we will be using document.querySelector
	/*Syntax: document.querySelector("html element")*/
	//the querySelector function takes a string input that is formatted like css selector when applying the styles

	const txtFirstName = document.querySelector("#txt-first-name");

	const txtLastName = document.querySelector("#txt-last-name")
	// console.log(txtFirstName);

	const name = document.querySelectorAll(".full-name");
	// console.log(name);

	const span = document.querySelectorAll("span");
	// console.log(span);

	const text = document.querySelectorAll('input[type]');
	// console.log(text);


	let spanFullName = document.querySelector('#fullName');

	/*const div = document.querySelectorAll(".first-div > span");
	console.log(div);
	const div = document.querySelectorAll(".first-div > .second-span");
	console.log(div);*/

//section : EVENT LISTENERS
	//	Whenever a user interacts with a webpage, this action is considered as an event. 
	// working with events is large part of creatig interactivity in a webpage.
	//specific fuctions that will perform an action

//The function use ia "addEventListener", it takes twi arguments
	// first argument a string identifying a event.
	//second argument, function that the listener will trigger once the "specific event" is triggered.

	/*txtFirstName.addEventListener("keyup", (event) =>{
		console.log(event);
	});*/

	/*txtFirstName.addEventListener("change", (event) =>{
		console.log(event.target.value);
	});*/

	txtFirstName.addEventListener("keydown", (event) =>{
		console.log(event.target.value);

	});

	

	const fullName = () =>{
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}` 
	}

	txtFirstName.addEventListener("keyup", fullName);
	txtLastName.addEventListener("keyup", fullName);


		let selectColor = document.getElementById("color");
		
		selectColor.addEventListener("change", () => {
		spanFullName.style.color = selectColor.value;
		});
